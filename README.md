# Helm Package Registry

Demonstrating the release of a helm chart to the GitLab own package registry

You can find the helm package in the menu under "Packages & Registries" -> "Package Registry"

More information in the docs: https://docs.gitlab.com/ee/user/packages/helm_repository/
